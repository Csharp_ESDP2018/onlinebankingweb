﻿using InternetBanking.Models;
using InternetBanking.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace InternetBanking.Tests
{
    public class IExchangeRateServiceTests
    {
        public ApplicationContext context;
        public IExchangeRateService exchangeRateService;
        public IExchangeRateServiceTests()
        {
            context = ConnectService.GetContext();
            exchangeRateService = new ExchangeRateService(context);
        }

        [Fact]
        public void AddExchangeRate()
        {
            ExchangeRate exchangeRate = new ExchangeRate()
            {
                CurrencyId = 2,
                Rate = 75,
                RateDate = DateTime.Now
            };

            exchangeRateService.AddExchangeRate(exchangeRate);
            bool rateIsExist = exchangeRateService.IsExist(exchangeRate.Id);
            Assert.True(rateIsExist);
        }

        [Fact]
        public void FindExchangeRateById()
        {
            bool currencyIsExist = exchangeRateService.IsExist(GetLastExchangeRate().Id);
            Assert.True(currencyIsExist);
        }

        [Fact]
        public void GetExchangeRates()
        {
            List<ExchangeRate> exchangeList = exchangeRateService.GetExchangeRates();
            Assert.Equal(exchangeList, context.ExchangeRates.ToList());
        }

        [Fact]
        public void GetLastExchangeRatesByDate()
        {
            IQueryable<ExchangeRate> exchangeList = exchangeRateService.GetLastExchangeRatesByDate();
            bool IsInList = exchangeList.Any(r => r.RateDate == GetLastExchangeRate().RateDate);
            Assert.True(IsInList);
        }

        [Fact]
        public void RemoveExchangeRate()
        {
            ExchangeRate exchangeRate = GetLastExchangeRate();
            int ExchangeRateId = exchangeRate.Id;
            exchangeRateService.RemoveExchangeRate(exchangeRate);
            bool rateIsExist = exchangeRateService.IsExist(ExchangeRateId);
            Assert.False(rateIsExist);
        }

        [Fact]
        public void UpdateExchangeRate()
        {
            ExchangeRate exchangeRate = GetLastExchangeRate();
            exchangeRate.Rate = 65;
            exchangeRateService.UpdateExchangeRate(exchangeRate);
            Assert.Equal(65, exchangeRate.Rate);
        }

        public ExchangeRate GetLastExchangeRate()
        {
            return context.ExchangeRates.OrderByDescending(r => r.Id).First();
        }
    }
}
