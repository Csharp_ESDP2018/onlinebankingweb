﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InternetBanking.Models;
using InternetBanking.Services;
using Microsoft.IdentityModel.Protocols;
using Moq;
using Xunit;

namespace InternetBanking.Tests
{
    public class AccountServiceTests
    {
        public ApplicationContext context;
        public AccountServiceTests()
        {
            context = ConnectService.GetContext();
        }


        [Fact]
        public void GenerationRandNumber()
        {
            
            AccountService service = new AccountService(context);
            string randNumber = service.GetUniqueAccountNumber();
            Assert.Equal(16, randNumber.Length);
        }

        [Fact]
        public void IsReturnedAccountNumberUnique()
        {

            AccountService service = new AccountService(context);
            string result = service.GetUniqueAccountNumber();
            Assert.Equal(16, result.Length);

            Assert.DoesNotContain(result, GetAccountNumbers());
        }

        [Fact]
        public void IsCreateAccountForUserNotNull()
        {
            AccountService service = new AccountService(context);

            Account account = service.CreateAccount(new User(), new Currency());
            Assert.NotNull(account);

        }
        [Fact]
        public void AddAccountCheckDb()
        {
            AccountService service = new AccountService(context);
            Account account = service.CreateAccount(new User(), new Currency());
            service.AddAccount(account);
            Account accountInDb = context.Accounts.FirstOrDefault(a => a.Id == account.Id);
            Assert.Equal(accountInDb, account);

        }

        [Fact]
        public void IsCreateAccountForCompanyNotNull()
        {
            AccountService service = new AccountService(context);

            Account account = service.CreateAccount(new Company(), new Currency());
            Assert.NotNull(account);

        }

        private ICollection<string> GetAccountNumbers()
        {
            return context.Accounts.Select(a => a.Number).ToList();
        }


    }
}
