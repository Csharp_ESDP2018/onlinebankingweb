﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using InternetBanking.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace InternetBanking.Tests
{
    public static class ConnectService
    {
        public static ApplicationContext GetContext()
        {
            DirectoryInfo path = new DirectoryInfo(@"..\..\..");
            //IConfigurationRoot Configuration = new ConfigurationBuilder()
            IConfigurationRoot Configuration = new ConfigurationBuilder()
            .SetBasePath(path.FullName)
            .AddJsonFile("appsettings.json")
            .Build();

            var optionsBuilder = new DbContextOptionsBuilder<ApplicationContext>();
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));

            return new ApplicationContext(optionsBuilder.Options);

            
        }
    }
}
