﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternetBanking.Models;

namespace InternetBanking.Services
{
    public class TransferService : ITransferService
    {
        private readonly ApplicationContext context;
        private readonly ITransactionService transactionService;

        public TransferService(ApplicationContext context, ITransactionService transactionService)
        {
            this.context = context;
            this.transactionService = transactionService;
        }

        public InnerTransfer CreateInnerTransfer(Account sender, Account receiver, double amount, string comment)
        {
            InnerTransfer transfer = new InnerTransfer
            {
                AccountSender = sender,
                AccountReceiver = receiver,
                Amount = amount,
                Comment = comment
            };
            return transfer;
        }

        public async void AddTransfer(InnerTransfer transfer)
        {
            await context.AddAsync(transfer);
            List<Transaction> transactions =  transactionService.CreateTransactions(transfer);
            transactionService.AddTransactions(transactions);
            context.SaveChanges();
        }
    }
}
