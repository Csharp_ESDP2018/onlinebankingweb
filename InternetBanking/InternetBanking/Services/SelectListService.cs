﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternetBanking.Models;
using InternetBanking.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace InternetBanking.Services
{
    public class SelectListService : ISelectListService
    {
        private readonly ApplicationContext context;

        public SelectListService(ApplicationContext context)
        {
            this.context = context;
        }

        public SelectList GetCountries()
        {
            

            return new SelectList(context.Countries.ToList(), "Id", "CountryName");
        }



        

        public SelectList GetLegalForms()
        {
            return new SelectList(context.LegalForms.ToList(), "Id", "LegalFormName"); 
        }

        public SelectList GetPropertyTypes()
        {
            return new SelectList(context.PropertyTypes.ToList(), "Id", "PropertyTypeName");
        }

        public SelectList GetResidencies()
        {
            return new SelectList(context.Residencies.ToList(), "Id", "ResidencyName");
        }

        public SelectList GetTaxInspections()
        {
            return new SelectList(context.TaxInspections.ToList(), "Id", "TaxInspectionName");
        }

        public SelectList GetCurrencies()
        {
            return new SelectList(context.Currencies.ToList(), "Id", "Name");
        }

        public SelectList GetUserAccounts(int userInfoId)
        {
            return new SelectList(context.Accounts.Where(a=>a.UserInfoId == userInfoId), "Id", "Number");
        }

        public AddCompanyViewModel GetCompaniesSelectList(AddCompanyViewModel company)
        {
            company.Countries = GetCountries(); 
            company.LegalForms = GetLegalForms(); 
            company.PropertyTypes = GetPropertyTypes();
            company.Residencies = GetResidencies(); 
            company.TaxInspections = GetTaxInspections();
            return company;
        }

        public SelectList GetTypeOfDocuments()
        {
            return new SelectList(context.TypeOfDocuments.ToList(), "Id", "Name");
        }
    }
}
