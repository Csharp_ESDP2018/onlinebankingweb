﻿using InternetBanking.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetBanking.Services
{
    public class ExchangeRateService : IExchangeRateService
    {
        private readonly ApplicationContext context;

        public ExchangeRateService(ApplicationContext context)
        {
            this.context = context;
        }

        public void AddExchangeRate(ExchangeRate exchangeRate)
        {
            context.ExchangeRates.Add(exchangeRate);
            context.SaveChanges();
        }

        public ExchangeRate FindExchangeRateById(int id)
        {
            ExchangeRate exchangeRate = context.ExchangeRates.FirstOrDefault(r => r.Id == id);
            return exchangeRate;
        }

        public List<ExchangeRate> GetExchangeRates()
        {
            return context.ExchangeRates.ToList();
        }

        public IQueryable<ExchangeRate> GetLastExchangeRatesByDate()
        {
            var exchangeRatesMaxDate = from rateTable in context.ExchangeRates
                                       group rateTable by new { rateTable.CurrencyId, rateTable.ExchangeRateTypeId } into g
                                       select new { CurrencyId = g.Key.CurrencyId, ExchangeRateTypeId = g.Key.ExchangeRateTypeId,  RateDate = g.Max(ratedate => ratedate.RateDate)};

            IQueryable<ExchangeRate> exchangeRates = from rateTableMax in exchangeRatesMaxDate
                                                     join rateTable in context.ExchangeRates
                                                     on new { rateTableMax.CurrencyId, rateTableMax.RateDate, rateTableMax.ExchangeRateTypeId } equals
                                                        new { rateTable.CurrencyId, rateTable.RateDate, rateTable.ExchangeRateTypeId } into joined
                                                     from j in joined.DefaultIfEmpty()
                                                     select new ExchangeRate()
                                                     {
                                                         RateDate = rateTableMax.RateDate,
                                                         CurrencyId = j.CurrencyId,
                                                         Rate = j.Rate,
                                                         Id = j.Id,
                                                         Currency = j.Currency,
                                                         ExchangeRateTypeId = j.ExchangeRateTypeId,
                                                         ExchangeRateType = j.ExchangeRateType
                                                     };

            return exchangeRates;
        }

        public void RemoveExchangeRate(ExchangeRate exchangeRate)
        {
            context.ExchangeRates.Remove(exchangeRate);
            context.SaveChanges();
        }

        public void UpdateExchangeRate(ExchangeRate exchangeRate)
        {
            context.ExchangeRates.Update(exchangeRate);
            context.SaveChanges();
        }

        public bool IsExist(int id)
        {
            return FindExchangeRateById(id) != null;
        }

        public List<ExchangeRateType> GetExchangeRateTypes()
        {
            return context.ExchangeRateTypes.ToList();
        }


    }
}
