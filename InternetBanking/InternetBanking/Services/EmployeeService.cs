﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternetBanking.Models;
using InternetBanking.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace InternetBanking.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly UserManager<User> userManager;
        private ApplicationContext context;

        public EmployeeService(UserManager<User> userManager, ApplicationContext context)
        {
            this.userManager = userManager;
            this.context = context;
        }
        public async Task<IdentityResult> CreateEmployee(User user, RegisterEmployeeViewModel model)
        {

            return await userManager.CreateAsync(user, model.Password);
        }

        public EmployeeInfo CreateEmployeeInfo(User user, RegisterEmployeeViewModel model)
        {
            EmployeeInfo employee = new EmployeeInfo
            {
                UserId = user.Id,
                FirstName = model.FirstName,
                SecondName = model.SecondName,
                MiddleName = model.MiddleName,
                Position = model.Position,
                CompanyId = model.CompanyId
            };
            context.EmployeeInfos.Add(employee);
            context.SaveChanges();
            return employee;
        }

        public IQueryable<EmployeeInfo> GetEmployeesByCompanyId(int id)
        {
            IQueryable<EmployeeInfo> employees = context.EmployeeInfos.Include(e => e.Company).Where(e => e.CompanyId == id);

            return employees;
        }

        public EmployeeInfo FindEmployeeById(int id)
        {
            EmployeeInfo employee = context.EmployeeInfos.Include(e => e.Company).Include(e => e.User).FirstOrDefault(e => e.Id == id);

            return employee;
        }

        public void UpdateEmployee(User user, EmployeeEditViewModel model)
        {
            context.EmployeeInfos.Update(model.employee);
            context.Users.Update(user);
            context.SaveChanges();
        }
    }
}
