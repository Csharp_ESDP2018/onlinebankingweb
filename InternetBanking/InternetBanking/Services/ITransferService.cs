﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternetBanking.Models;

namespace InternetBanking.Services
{
    public interface ITransferService
    {
        void AddTransfer(InnerTransfer transfer);
        InnerTransfer CreateInnerTransfer(Account sender, Account receiver, double amount, string comment);
    }
}
