﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternetBanking.Models;

namespace InternetBanking.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ApplicationContext context;
        private readonly List<TransactionType> transactionTypes;

        public TransactionService(ApplicationContext context)
        {
            this.context = context;
            transactionTypes = context.TransactionTypes.ToList();
        }

        

        public List<Transaction> CreateTransactions(InnerTransfer transfer)
        {
            
            List<Transaction> transactions = new List<Transaction>();
            if (transfer.AccountReceiverId != null)
            {
               
                Transaction transaction = new Transaction
                {
                    AccountId = (int)transfer.AccountReceiverId,
                    Amount = transfer.Amount,
                    TransactionTypeId = transactionTypes.FirstOrDefault(tt=>tt.TypeName == "Debit").Id

                };
                transactions.Add(transaction);
            }
            if (transfer.AccountSenderId != null)
            {

                Transaction transaction = new Transaction
                {
                    AccountId = (int)transfer.AccountSenderId,
                    Amount = transfer.Amount,
                    TransactionTypeId = transactionTypes.FirstOrDefault(tt => tt.TypeName == "Credit").Id

                };
                transactions.Add(transaction);
            }
            return transactions;
        }

        public async void AddTransactions(List<Transaction> transactions)
        {
            await context.Transactions.AddRangeAsync(transactions);
            context.SaveChanges();
        }
    }
}
