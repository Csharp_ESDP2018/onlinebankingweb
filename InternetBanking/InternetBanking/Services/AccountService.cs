﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InternetBanking.Models;
using InternetBanking.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.ProjectModel;

namespace InternetBanking.Services
{
    public class AccountService : IAccountService
    {
        private const string BANK_CODE = "123";
        private readonly ApplicationContext context;

        public AccountService(ApplicationContext context)
        {
            this.context = context;
        }

        public Account CreateAccount(User user, Currency currency)
        {
            UserInfo userInfo = context.UserInfo.FirstOrDefault(ui => ui.UserId == user.Id);
            Account account = new Account
            {
                Number = GetUniqueAccountNumber(),
                UserInfo = userInfo,
                Currency = currency,

            };
            return account;

        }

        public Account CreateAccount(Company company, Currency currency)
        {
            Account account = new Account
            {
                Number = GetUniqueAccountNumber(),
                Company = company,
                Currency = currency

            };
            return account;

        }

        public async void AddAccount(Account account)
        {
            await context.Accounts.AddAsync(account);
            context.SaveChanges();
        }

        public string GetUniqueAccountNumber()
        {
            string number = string.Empty;
            bool isUnique = false;
            while (!isUnique)
            {
                number = GetAccountNumber(BANK_CODE);
                isUnique = IsUniqueAsync(number).Result;
            }
            return number;
        }





        private string GenerateRandomNumber()
        {
            Random rnd = new Random();
            string number = string.Empty;
            for (int i = 0; i < 11; i++)
            {
                int n = rnd.Next(0, 10);
                number += n;
            }
            return number;
        }

        private string GetAccountNumber(string bankCode)
        {
            string number = bankCode + GenerateRandomNumber();
            string suffix = GetRemainder(number);
            string result = number + suffix;
            return result;
        }

        private string GetRemainder(string number)
        {
            string remainder = (long.Parse(number) % 97).ToString();
            if (remainder == "0")
            {
                return "97";
            }
            if (remainder.Length == 1)
            {
                return "0" + remainder;
            }
            return remainder;
        }

        private async Task<bool> IsUniqueAsync(string number)
        {
            Account account = await context.Accounts.FirstOrDefaultAsync(a => a.Number == number);
            return account == null;
        }

        public List<AccountWithBalance> GetUserInfoAccounts(int id)
        {
            List<Account> accounts = context.Accounts.Where(u => u.UserInfoId == id).Include(u => u.Currency).ToList();
            List<AccountWithBalance> accountWithBalances = new List<AccountWithBalance>();
            foreach (var acc in accounts)
            {
                accountWithBalances.Add(new AccountWithBalance
                {
                    Account = acc,
                    Balance = GetAccountBalance(acc).Result

                });
            }
            return accountWithBalances;
        }

        public List<AccountWithBalance> GetCompanyAccounts(int id)
        {
            List<Account> accounts = context.Accounts.Where(u => u.CompanyId == id).Include(u => u.Currency).ToList();
            List<AccountWithBalance> accountWithBalances = new List<AccountWithBalance>();
            foreach (var acc in accounts)
            {
                accountWithBalances.Add(new AccountWithBalance
                {
                    Account = acc,
                    Balance = GetAccountBalance(acc).Result

                });
            }
            return accountWithBalances;
        }

        public List<Account> GetCompanyAccountsWithoutBalance(int id)
        {
            List<Account> accounts = context.Accounts.Where(u => u.CompanyId == id).Include(u => u.Currency).ToList();

            return accounts;
        }

        public List<Account> GetUserAccountsWithoutBalance(int id)
        {
            List<Account> accounts = context.Accounts.Where(u => u.UserInfoId == id).Include(u => u.Currency).ToList();

            return accounts;
        }

        public async Task<Account> FindAccountByNumber(string accountNumber)
        {
            Account account = await context.Accounts.FirstOrDefaultAsync(a => a.Number == accountNumber);
            return account;
        }

        public async Task<double> GetAccountBalance(Account account)
        {
            double Balance = await context.Transactions
                                 .Where(t => t.TransactionTypeId == 1 && t.AccountId == account.Id)
                                 .SumAsync(t => t.Amount) -
                             await context.Transactions
                                 .Where(t => t.TransactionTypeId == 2 && t.AccountId == account.Id)
                                 .SumAsync(t => t.Amount);
            return Math.Round(Balance, 2);
        }

        /*public async Task<double> GetAccountBalance(Account account)
        {
            List<Transaction> debitTransactions =
                await context.Transactions.Where(t => t.AccountId == account.Id && t.TransactionTypeId == 1).ToListAsync();
            List<Transaction> creditTransactions =
                await context.Transactions.Where(t => t.AccountId == account.Id && t.TransactionTypeId == 2).ToListAsync();
            double debit = 0;
            double credit = 0;
            foreach (var debitTransaction in debitTransactions)
            {
                debit += debitTransaction.Amount;
            }
            foreach (var creditTransaction in creditTransactions)
            {
                credit += creditTransaction.Amount;
            }
            return debit - credit;

        }*/

        public async Task<Account> FindAccountById(int accountId)
        {
            Account account = await context.Accounts.Include(a => a.Currency).Include(u=>u.UserInfo).ThenInclude(a=>a.User).FirstOrDefaultAsync(a => a.Id == accountId);
            return account;
        }

        public async Task<bool> IsAccountExist(string accountNumber)
        {
            Account account = await context.Accounts.FirstOrDefaultAsync(a => a.Number == accountNumber);
            return account != null;
        }

        public async Task<bool> CompareAccountsCurrencies(string receiverAccountNumber, int senderAccountId)
        {
            Account receiver = await FindAccountByNumber(receiverAccountNumber);
            Account sender = await FindAccountById(senderAccountId);
            return receiver.CurrencyId != sender.CurrencyId;
        }

        public async Task<bool> IsBalanceEnough(int accountId, double amount)
        {
            Account account = await FindAccountById(accountId);
            double balance = await GetAccountBalance(account);
            return amount < balance;
        }

        public async Task<bool> IsAccountSenderNotReceiver(string accountNumber, User user)
        {
            UserInfo userInfo = await context.UserInfo.FirstOrDefaultAsync(ui => ui.UserId == user.Id);
            List<Account> userAccounts = await context.Accounts.Where(a => a.UserInfoId == userInfo.Id).ToListAsync();
            Account account = await FindAccountByNumber(accountNumber);
            return userAccounts.Exists(a =>a == account);
        }

        public bool AmountTryParse(string modelAmount, out double amount)
        {
            if (!string.IsNullOrEmpty(modelAmount))
            {
                modelAmount = modelAmount.Replace('.', ',');
            }
            bool tryParse =  double.TryParse(modelAmount, out amount);
            amount = Math.Round(amount, 2);
           
            return tryParse;
        }

      
    }
}
