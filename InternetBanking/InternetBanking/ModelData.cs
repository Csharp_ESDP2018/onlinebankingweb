﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternetBanking.Models;
using InternetBanking.Models.SelectTable;

namespace InternetBanking
{
    public class ModelData
    {
        public static void FillData(ApplicationContext context)
        {
            if (!context.TypeOfDocuments.Any())
            {
                TypeOfDocument pasport = new TypeOfDocument{Name = "Паспорт"};
                TypeOfDocument pasport1 = new TypeOfDocument { Name = "ВоенныйБилет" };
                TypeOfDocument pasport2 = new TypeOfDocument { Name = "СвидетельствоОрождении" };
                context.TypeOfDocuments.AddRange(pasport, pasport1, pasport2);
                context.SaveChanges();
            }

            if (!context.Countries.Any())
            {
                Country country = new Country { CountryName = "Кыргызстан" };
                Country country1 = new Country { CountryName = "Таджикистан" };
                Country country2 = new Country { CountryName = "Казахстан" };
                context.Countries.AddRange(country, country1, country2);
                context.SaveChanges();
            }
            if (!context.LegalForms.Any())
            {
                LegalForm legalForm = new LegalForm { LegalFormName = "ОсОО" };
                LegalForm legalForm1= new LegalForm { LegalFormName = "ОАО" };
                LegalForm legalForm2 = new LegalForm { LegalFormName = "ЗАО" };
                context.LegalForms.AddRange(legalForm, legalForm1, legalForm2);
                context.SaveChanges();
            }
            if (!context.PropertyTypes.Any())
            {
                PropertyType propertyType = new PropertyType { PropertyTypeName = "Государственная" };
                PropertyType propertyType1 = new PropertyType { PropertyTypeName = "Частная" };
                PropertyType propertyType2 = new PropertyType { PropertyTypeName = "Смешанная" };
                context.PropertyTypes.AddRange(propertyType, propertyType1, propertyType2);
                context.SaveChanges();
            }
            if (!context.Residencies.Any())
            {
                Residency residency = new Residency { ResidencyName = "Резидент" };
                Residency residency1 = new Residency { ResidencyName = "НеРезидент" };
           
                context.Residencies.AddRange(residency, residency1);
                context.SaveChanges();
            }
            if (!context.TaxInspections.Any())
            {
                TaxInspection taxInspection = new TaxInspection { TaxInspectionName = "Ленинский" };
                TaxInspection taxInspection1 = new TaxInspection { TaxInspectionName = "Свердловский" };
                TaxInspection taxInspection2= new TaxInspection { TaxInspectionName = "Октябрьский" };
                context.TaxInspections.AddRange(taxInspection, taxInspection1, taxInspection2);
                context.SaveChanges();
            }
            if (!context.TransactionTypes.Any())
            {
                TransactionType transactionType = new TransactionType { TypeName = "Debit" };
                TransactionType transactionType1 = new TransactionType { TypeName = "Credit" };
                
                context.TransactionTypes.AddRange(transactionType, transactionType1);
                context.SaveChanges();
            }
            if (!context.ExchangeRateTypes.Any())
            {
                ExchangeRateType exchangeRateType = new ExchangeRateType { Name = "НБКР" };
                ExchangeRateType exchangeRateType1 = new ExchangeRateType { Name = "Рыночный" };

                context.ExchangeRateTypes.AddRange(exchangeRateType, exchangeRateType1);
                context.SaveChanges();
            }
        }
    }
}
