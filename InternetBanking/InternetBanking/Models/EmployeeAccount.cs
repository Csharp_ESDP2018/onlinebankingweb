﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InternetBanking.Models
{
    public class EmployeeAccount
    {
        [Key]
        public int Id { get; set; }

        public int EmployeeId { get; set; }
        public EmployeeInfo Employee { get; set; }

        public int AccountId { get; set; }
        public Account Account { get; set; }

        public bool RightOfConfirmation { get; set; }

        public bool RightOfCreate { get; set; }
    }
}
