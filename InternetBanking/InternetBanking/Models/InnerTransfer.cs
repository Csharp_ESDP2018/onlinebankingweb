﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InternetBanking.Models
{
    public class InnerTransfer
    {
        [Key]
        public int Id { get; set;}

        public int? AccountSenderId { get; set; }
        public Account AccountSender { get; set; }

        public int?  AccountReceiverId { get; set; }
        public Account AccountReceiver { get; set; }

        public double Amount { get; set; }
        
        public string Comment { get; set; }
    }
}
