﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace InternetBanking.Migrations
{
    public partial class RenameCountryIdToCitezenshipId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserInfo_Countries_CountryId",
                table: "UserInfo");

            migrationBuilder.DropIndex(
                name: "IX_UserInfo_CountryId",
                table: "UserInfo");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "UserInfo");

            migrationBuilder.AddColumn<int>(
                name: "CitezenshipId",
                table: "UserInfo",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserInfo_CitezenshipId",
                table: "UserInfo",
                column: "CitezenshipId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserInfo_Countries_CitezenshipId",
                table: "UserInfo",
                column: "CitezenshipId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserInfo_Countries_CitezenshipId",
                table: "UserInfo");

            migrationBuilder.DropIndex(
                name: "IX_UserInfo_CitezenshipId",
                table: "UserInfo");

            migrationBuilder.DropColumn(
                name: "CitezenshipId",
                table: "UserInfo");

            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "UserInfo",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserInfo_CountryId",
                table: "UserInfo",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserInfo_Countries_CountryId",
                table: "UserInfo",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
