﻿// <auto-generated />
using InternetBanking.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;

namespace InternetBanking.Migrations
{
    [DbContext(typeof(ApplicationContext))]
    [Migration("20180125084725_AddCompanyModelAndBankModel")]
    partial class AddCompanyModelAndBankModel
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("InternetBanking.Models.Account", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Balance");

                    b.Property<string>("Number");

                    b.Property<int>("UserId");

                    b.Property<string>("UserId1");

                    b.HasKey("Id");

                    b.HasIndex("UserId1");

                    b.ToTable("Accounts");
                });

            modelBuilder.Entity("InternetBanking.Models.Address", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City");

                    b.Property<string>("Country");

                    b.Property<string>("HouseAddress");

                    b.Property<string>("PostCode");

                    b.Property<string>("Street");

                    b.Property<string>("TypeOfAddress");

                    b.Property<int?>("UserInfoId");

                    b.HasKey("Id");

                    b.HasIndex("UserInfoId");

                    b.ToTable("Addresses");
                });

            modelBuilder.Entity("InternetBanking.Models.CompanyInfo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CodeOKPO");

                    b.Property<int>("CountryId");

                    b.Property<string>("INN");

                    b.Property<int>("LegalFormId");

                    b.Property<string>("NameCompany");

                    b.Property<int>("NumberOfEmployees");

                    b.Property<int>("PropertyTypeId");

                    b.Property<int>("RegistrationDataId");

                    b.Property<string>("RegistrationNumberSocialFund");

                    b.Property<int>("ResidencyId");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("CountryId");

                    b.HasIndex("LegalFormId");

                    b.HasIndex("PropertyTypeId");

                    b.HasIndex("RegistrationDataId");

                    b.HasIndex("ResidencyId");

                    b.HasIndex("UserId");

                    b.ToTable("CompaniesInfo");
                });

            modelBuilder.Entity("InternetBanking.Models.Currency", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Code");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Currencies");
                });

            modelBuilder.Entity("InternetBanking.Models.ExchangeRate", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CurrencyId");

                    b.Property<double>("Rate");

                    b.Property<DateTime>("RateDate");

                    b.HasKey("Id");

                    b.HasIndex("CurrencyId");

                    b.ToTable("ExchangeRates");
                });

            modelBuilder.Entity("InternetBanking.Models.PassportInfo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateofExtradition");

                    b.Property<string>("IssuedBy");

                    b.Property<int>("Number");

                    b.Property<string>("Series");

                    b.Property<int>("TypeOfDocumentId");

                    b.Property<DateTime>("Validaty");

                    b.HasKey("Id");

                    b.HasIndex("TypeOfDocumentId");

                    b.ToTable("PassportsInfo");
                });

            modelBuilder.Entity("InternetBanking.Models.RegistrationData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateOfInitialRegistration");

                    b.Property<DateTime>("DateOfRegistrationMinistryJustice");

                    b.Property<string>("IssuedBy");

                    b.Property<string>("RegistrationAuthority");

                    b.Property<int>("TaxInspectionId");

                    b.HasKey("Id");

                    b.HasIndex("TaxInspectionId");

                    b.ToTable("RegistrationDatas");
                });

            modelBuilder.Entity("InternetBanking.Models.SelectTable.Country", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CountryName");

                    b.HasKey("Id");

                    b.ToTable("Countries");
                });

            modelBuilder.Entity("InternetBanking.Models.SelectTable.LegalForm", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LegalFormName");

                    b.HasKey("Id");

                    b.ToTable("LegalForms");
                });

            modelBuilder.Entity("InternetBanking.Models.SelectTable.PropertyType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("PropertyTypeName");

                    b.HasKey("Id");

                    b.ToTable("PropertyTypes");
                });

            modelBuilder.Entity("InternetBanking.Models.SelectTable.Residency", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ResidencyName");

                    b.HasKey("Id");

                    b.ToTable("Residencies");
                });

            modelBuilder.Entity("InternetBanking.Models.SelectTable.TaxInspection", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("TaxInspectionName");

                    b.HasKey("Id");

                    b.ToTable("TaxInspections");
                });

            modelBuilder.Entity("InternetBanking.Models.TypeOfDocument", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("TypeOfDocuments");
                });

            modelBuilder.Entity("InternetBanking.Models.User", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("InternetBanking.Models.UserInfo", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("BirthDay");

                    b.Property<string>("Citizenship");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("Gender");

                    b.Property<string>("Inn");

                    b.Property<string>("MiddleName");

                    b.Property<int>("PassportInfoId");

                    b.Property<string>("SecondName");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("PassportInfoId");

                    b.HasIndex("UserId");

                    b.ToTable("UserInfo");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("InternetBanking.Models.Account", b =>
                {
                    b.HasOne("InternetBanking.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId1");
                });

            modelBuilder.Entity("InternetBanking.Models.Address", b =>
                {
                    b.HasOne("InternetBanking.Models.UserInfo", "UserInfo")
                        .WithMany("Addresses")
                        .HasForeignKey("UserInfoId");
                });

            modelBuilder.Entity("InternetBanking.Models.CompanyInfo", b =>
                {
                    b.HasOne("InternetBanking.Models.SelectTable.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("InternetBanking.Models.SelectTable.LegalForm", "LegalForm")
                        .WithMany()
                        .HasForeignKey("LegalFormId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("InternetBanking.Models.SelectTable.PropertyType", "PropertyType")
                        .WithMany()
                        .HasForeignKey("PropertyTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("InternetBanking.Models.RegistrationData", "RegistrationData")
                        .WithMany()
                        .HasForeignKey("RegistrationDataId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("InternetBanking.Models.SelectTable.Residency", "Residency")
                        .WithMany()
                        .HasForeignKey("ResidencyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("InternetBanking.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("InternetBanking.Models.ExchangeRate", b =>
                {
                    b.HasOne("InternetBanking.Models.Currency", "Currency")
                        .WithMany()
                        .HasForeignKey("CurrencyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("InternetBanking.Models.PassportInfo", b =>
                {
                    b.HasOne("InternetBanking.Models.TypeOfDocument", "TypeOfDocument")
                        .WithMany()
                        .HasForeignKey("TypeOfDocumentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("InternetBanking.Models.RegistrationData", b =>
                {
                    b.HasOne("InternetBanking.Models.SelectTable.TaxInspection", "TaxInspection")
                        .WithMany()
                        .HasForeignKey("TaxInspectionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("InternetBanking.Models.UserInfo", b =>
                {
                    b.HasOne("InternetBanking.Models.PassportInfo", "PassportInfo")
                        .WithMany()
                        .HasForeignKey("PassportInfoId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("InternetBanking.Models.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("InternetBanking.Models.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("InternetBanking.Models.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("InternetBanking.Models.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("InternetBanking.Models.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
