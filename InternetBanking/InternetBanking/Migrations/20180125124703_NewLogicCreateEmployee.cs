﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace InternetBanking.Migrations
{
    public partial class NewLogicCreateEmployee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_AspNetUsers_UserId1",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_CompaniesInfo_AspNetUsers_UserId",
                table: "CompaniesInfo");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_UserId1",
                table: "Accounts");

            migrationBuilder.DropIndex(
                name: "IX_CompaniesInfo_UserId",
                table: "CompaniesInfo");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "CompaniesInfo");

            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "Accounts",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserInfoId",
                table: "Accounts",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "EmployeeInfos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyId = table.Column<int>(type: "int", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MiddleName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Position = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecondName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeInfos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmployeeInfos_CompaniesInfo_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "CompaniesInfo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeeInfos_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_CompanyId",
                table: "Accounts",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_UserInfoId",
                table: "Accounts",
                column: "UserInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeInfos_CompanyId",
                table: "EmployeeInfos",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeInfos_UserId",
                table: "EmployeeInfos",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_CompaniesInfo_CompanyId",
                table: "Accounts",
                column: "CompanyId",
                principalTable: "CompaniesInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_UserInfo_UserInfoId",
                table: "Accounts",
                column: "UserInfoId",
                principalTable: "UserInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_CompaniesInfo_CompanyId",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_UserInfo_UserInfoId",
                table: "Accounts");

            migrationBuilder.DropTable(
                name: "EmployeeInfos");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_CompanyId",
                table: "Accounts");

            migrationBuilder.DropIndex(
                name: "IX_Accounts_UserInfoId",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "UserInfoId",
                table: "Accounts");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Accounts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "UserId1",
                table: "Accounts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "CompaniesInfo",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_UserId1",
                table: "Accounts",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_CompaniesInfo_UserId",
                table: "CompaniesInfo",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_AspNetUsers_UserId1",
                table: "Accounts",
                column: "UserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CompaniesInfo_AspNetUsers_UserId",
                table: "CompaniesInfo",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
