﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace InternetBanking.Migrations
{
    public partial class EditUserInfoCitezenshipToCountryRel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Countries_CountryId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_LegalForms_LegalFormId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_PropertyTypes_PropertyTypeId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Residencies_ResidencyId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_ExchangeRates_Currencies_CurrencyId",
                table: "ExchangeRates");

            migrationBuilder.DropForeignKey(
                name: "FK_RegistrationDatas_TaxInspections_TaxInspectionId",
                table: "RegistrationDatas");

            migrationBuilder.DropColumn(
                name: "Citizenship",
                table: "UserInfo");

            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "UserInfo",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TaxInspectionId",
                table: "RegistrationDatas",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CurrencyId",
                table: "ExchangeRates",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ResidencyId",
                table: "Companies",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PropertyTypeId",
                table: "Companies",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "LegalFormId",
                table: "Companies",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "Companies",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_UserInfo_CountryId",
                table: "UserInfo",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Countries_CountryId",
                table: "Companies",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_LegalForms_LegalFormId",
                table: "Companies",
                column: "LegalFormId",
                principalTable: "LegalForms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_PropertyTypes_PropertyTypeId",
                table: "Companies",
                column: "PropertyTypeId",
                principalTable: "PropertyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Residencies_ResidencyId",
                table: "Companies",
                column: "ResidencyId",
                principalTable: "Residencies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ExchangeRates_Currencies_CurrencyId",
                table: "ExchangeRates",
                column: "CurrencyId",
                principalTable: "Currencies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RegistrationDatas_TaxInspections_TaxInspectionId",
                table: "RegistrationDatas",
                column: "TaxInspectionId",
                principalTable: "TaxInspections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserInfo_Countries_CountryId",
                table: "UserInfo",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Countries_CountryId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_LegalForms_LegalFormId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_PropertyTypes_PropertyTypeId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Residencies_ResidencyId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_ExchangeRates_Currencies_CurrencyId",
                table: "ExchangeRates");

            migrationBuilder.DropForeignKey(
                name: "FK_RegistrationDatas_TaxInspections_TaxInspectionId",
                table: "RegistrationDatas");

            migrationBuilder.DropForeignKey(
                name: "FK_UserInfo_Countries_CountryId",
                table: "UserInfo");

            migrationBuilder.DropIndex(
                name: "IX_UserInfo_CountryId",
                table: "UserInfo");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "UserInfo");

            migrationBuilder.AddColumn<string>(
                name: "Citizenship",
                table: "UserInfo",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TaxInspectionId",
                table: "RegistrationDatas",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CurrencyId",
                table: "ExchangeRates",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ResidencyId",
                table: "Companies",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PropertyTypeId",
                table: "Companies",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LegalFormId",
                table: "Companies",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "Companies",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Countries_CountryId",
                table: "Companies",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_LegalForms_LegalFormId",
                table: "Companies",
                column: "LegalFormId",
                principalTable: "LegalForms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_PropertyTypes_PropertyTypeId",
                table: "Companies",
                column: "PropertyTypeId",
                principalTable: "PropertyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Residencies_ResidencyId",
                table: "Companies",
                column: "ResidencyId",
                principalTable: "Residencies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ExchangeRates_Currencies_CurrencyId",
                table: "ExchangeRates",
                column: "CurrencyId",
                principalTable: "Currencies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RegistrationDatas_TaxInspections_TaxInspectionId",
                table: "RegistrationDatas",
                column: "TaxInspectionId",
                principalTable: "TaxInspections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
