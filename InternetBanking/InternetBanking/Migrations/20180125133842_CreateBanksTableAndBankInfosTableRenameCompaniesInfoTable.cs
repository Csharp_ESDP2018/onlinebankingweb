﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace InternetBanking.Migrations
{
    public partial class CreateBanksTableAndBankInfosTableRenameCompaniesInfoTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_CompaniesInfo_CompanyId",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_CompaniesInfo_Countries_CountryId",
                table: "CompaniesInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_CompaniesInfo_LegalForms_LegalFormId",
                table: "CompaniesInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_CompaniesInfo_PropertyTypes_PropertyTypeId",
                table: "CompaniesInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_CompaniesInfo_RegistrationDatas_RegistrationDataId",
                table: "CompaniesInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_CompaniesInfo_Residencies_ResidencyId",
                table: "CompaniesInfo");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeInfos_CompaniesInfo_CompanyId",
                table: "EmployeeInfos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompaniesInfo",
                table: "CompaniesInfo");

            migrationBuilder.RenameTable(
                name: "CompaniesInfo",
                newName: "Companies");

            migrationBuilder.RenameIndex(
                name: "IX_CompaniesInfo_ResidencyId",
                table: "Companies",
                newName: "IX_Companies_ResidencyId");

            migrationBuilder.RenameIndex(
                name: "IX_CompaniesInfo_RegistrationDataId",
                table: "Companies",
                newName: "IX_Companies_RegistrationDataId");

            migrationBuilder.RenameIndex(
                name: "IX_CompaniesInfo_PropertyTypeId",
                table: "Companies",
                newName: "IX_Companies_PropertyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_CompaniesInfo_LegalFormId",
                table: "Companies",
                newName: "IX_Companies_LegalFormId");

            migrationBuilder.RenameIndex(
                name: "IX_CompaniesInfo_CountryId",
                table: "Companies",
                newName: "IX_Companies_CountryId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Companies",
                table: "Companies",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "BankInfos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankInfos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Banks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BankInfoId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Banks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Banks_BankInfos_BankInfoId",
                        column: x => x.BankInfoId,
                        principalTable: "BankInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Banks_BankInfoId",
                table: "Banks",
                column: "BankInfoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_Companies_CompanyId",
                table: "Accounts",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Countries_CountryId",
                table: "Companies",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_LegalForms_LegalFormId",
                table: "Companies",
                column: "LegalFormId",
                principalTable: "LegalForms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_PropertyTypes_PropertyTypeId",
                table: "Companies",
                column: "PropertyTypeId",
                principalTable: "PropertyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_RegistrationDatas_RegistrationDataId",
                table: "Companies",
                column: "RegistrationDataId",
                principalTable: "RegistrationDatas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Residencies_ResidencyId",
                table: "Companies",
                column: "ResidencyId",
                principalTable: "Residencies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeInfos_Companies_CompanyId",
                table: "EmployeeInfos",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_Companies_CompanyId",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Countries_CountryId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_LegalForms_LegalFormId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_PropertyTypes_PropertyTypeId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_RegistrationDatas_RegistrationDataId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies_Residencies_ResidencyId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeInfos_Companies_CompanyId",
                table: "EmployeeInfos");

            migrationBuilder.DropTable(
                name: "Banks");

            migrationBuilder.DropTable(
                name: "BankInfos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Companies",
                table: "Companies");

            migrationBuilder.RenameTable(
                name: "Companies",
                newName: "CompaniesInfo");

            migrationBuilder.RenameIndex(
                name: "IX_Companies_ResidencyId",
                table: "CompaniesInfo",
                newName: "IX_CompaniesInfo_ResidencyId");

            migrationBuilder.RenameIndex(
                name: "IX_Companies_RegistrationDataId",
                table: "CompaniesInfo",
                newName: "IX_CompaniesInfo_RegistrationDataId");

            migrationBuilder.RenameIndex(
                name: "IX_Companies_PropertyTypeId",
                table: "CompaniesInfo",
                newName: "IX_CompaniesInfo_PropertyTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Companies_LegalFormId",
                table: "CompaniesInfo",
                newName: "IX_CompaniesInfo_LegalFormId");

            migrationBuilder.RenameIndex(
                name: "IX_Companies_CountryId",
                table: "CompaniesInfo",
                newName: "IX_CompaniesInfo_CountryId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompaniesInfo",
                table: "CompaniesInfo",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_CompaniesInfo_CompanyId",
                table: "Accounts",
                column: "CompanyId",
                principalTable: "CompaniesInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CompaniesInfo_Countries_CountryId",
                table: "CompaniesInfo",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompaniesInfo_LegalForms_LegalFormId",
                table: "CompaniesInfo",
                column: "LegalFormId",
                principalTable: "LegalForms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompaniesInfo_PropertyTypes_PropertyTypeId",
                table: "CompaniesInfo",
                column: "PropertyTypeId",
                principalTable: "PropertyTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompaniesInfo_RegistrationDatas_RegistrationDataId",
                table: "CompaniesInfo",
                column: "RegistrationDataId",
                principalTable: "RegistrationDatas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompaniesInfo_Residencies_ResidencyId",
                table: "CompaniesInfo",
                column: "ResidencyId",
                principalTable: "Residencies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeInfos_CompaniesInfo_CompanyId",
                table: "EmployeeInfos",
                column: "CompanyId",
                principalTable: "CompaniesInfo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
