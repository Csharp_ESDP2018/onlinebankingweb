﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace InternetBanking.Migrations
{
    public partial class AddExchangeRateType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ExchangeRateTypeId",
                table: "ExchangeRates",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ExchangeRateTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExchangeRateTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExchangeRates_ExchangeRateTypeId",
                table: "ExchangeRates",
                column: "ExchangeRateTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExchangeRates_ExchangeRateTypes_ExchangeRateTypeId",
                table: "ExchangeRates",
                column: "ExchangeRateTypeId",
                principalTable: "ExchangeRateTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExchangeRates_ExchangeRateTypes_ExchangeRateTypeId",
                table: "ExchangeRates");

            migrationBuilder.DropTable(
                name: "ExchangeRateTypes");

            migrationBuilder.DropIndex(
                name: "IX_ExchangeRates_ExchangeRateTypeId",
                table: "ExchangeRates");

            migrationBuilder.DropColumn(
                name: "ExchangeRateTypeId",
                table: "ExchangeRates");
        }
    }
}
