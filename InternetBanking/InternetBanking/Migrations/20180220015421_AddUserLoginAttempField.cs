﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace InternetBanking.Migrations
{
    public partial class AddUserLoginAttempField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RegistrationDatas_TaxInspections_TaxInspectionId",
                table: "RegistrationDatas");

            migrationBuilder.AlterColumn<int>(
                name: "TaxInspectionId",
                table: "RegistrationDatas",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "RegistrationAuthority",
                table: "RegistrationDatas",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "IssuedBy",
                table: "RegistrationDatas",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "LoginAttemptsCount",
                table: "AspNetUsers",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_RegistrationDatas_TaxInspections_TaxInspectionId",
                table: "RegistrationDatas",
                column: "TaxInspectionId",
                principalTable: "TaxInspections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RegistrationDatas_TaxInspections_TaxInspectionId",
                table: "RegistrationDatas");

            migrationBuilder.DropColumn(
                name: "LoginAttemptsCount",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<int>(
                name: "TaxInspectionId",
                table: "RegistrationDatas",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "RegistrationAuthority",
                table: "RegistrationDatas",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "IssuedBy",
                table: "RegistrationDatas",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_RegistrationDatas_TaxInspections_TaxInspectionId",
                table: "RegistrationDatas",
                column: "TaxInspectionId",
                principalTable: "TaxInspections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
