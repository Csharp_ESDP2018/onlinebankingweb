﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace InternetBanking.Migrations
{
    public partial class AddContactInfoModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ContactInfoId",
                table: "UserInfo",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ContactInfoId",
                table: "EmployeeInfos",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ContactInfoId",
                table: "Companies",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ContactInfos",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CityPhone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FullName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MobilePhone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactInfos", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserInfo_ContactInfoId",
                table: "UserInfo",
                column: "ContactInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeInfos_ContactInfoId",
                table: "EmployeeInfos",
                column: "ContactInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_ContactInfoId",
                table: "Companies",
                column: "ContactInfoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_ContactInfos_ContactInfoId",
                table: "Companies",
                column: "ContactInfoId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeInfos_ContactInfos_ContactInfoId",
                table: "EmployeeInfos",
                column: "ContactInfoId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserInfo_ContactInfos_ContactInfoId",
                table: "UserInfo",
                column: "ContactInfoId",
                principalTable: "ContactInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Companies_ContactInfos_ContactInfoId",
                table: "Companies");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeInfos_ContactInfos_ContactInfoId",
                table: "EmployeeInfos");

            migrationBuilder.DropForeignKey(
                name: "FK_UserInfo_ContactInfos_ContactInfoId",
                table: "UserInfo");

            migrationBuilder.DropTable(
                name: "ContactInfos");

            migrationBuilder.DropIndex(
                name: "IX_UserInfo_ContactInfoId",
                table: "UserInfo");

            migrationBuilder.DropIndex(
                name: "IX_EmployeeInfos_ContactInfoId",
                table: "EmployeeInfos");

            migrationBuilder.DropIndex(
                name: "IX_Companies_ContactInfoId",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "ContactInfoId",
                table: "UserInfo");

            migrationBuilder.DropColumn(
                name: "ContactInfoId",
                table: "EmployeeInfos");

            migrationBuilder.DropColumn(
                name: "ContactInfoId",
                table: "Companies");
        }
    }
}
