﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace InternetBanking.Migrations
{
    public partial class AddBlackListOfPasswords : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RegistrationDatas_TaxInspections_TaxInspectionId",
                table: "RegistrationDatas");

            migrationBuilder.AlterColumn<int>(
                name: "TaxInspectionId",
                table: "RegistrationDatas",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "RegistrationAuthority",
                table: "RegistrationDatas",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "IssuedBy",
                table: "RegistrationDatas",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "BlackListedPasswords",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BlackListedPassword = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlackListedPasswords", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_RegistrationDatas_TaxInspections_TaxInspectionId",
                table: "RegistrationDatas",
                column: "TaxInspectionId",
                principalTable: "TaxInspections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RegistrationDatas_TaxInspections_TaxInspectionId",
                table: "RegistrationDatas");

            migrationBuilder.DropTable(
                name: "BlackListedPasswords");

            migrationBuilder.AlterColumn<int>(
                name: "TaxInspectionId",
                table: "RegistrationDatas",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "RegistrationAuthority",
                table: "RegistrationDatas",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "IssuedBy",
                table: "RegistrationDatas",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddForeignKey(
                name: "FK_RegistrationDatas_TaxInspections_TaxInspectionId",
                table: "RegistrationDatas",
                column: "TaxInspectionId",
                principalTable: "TaxInspections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
