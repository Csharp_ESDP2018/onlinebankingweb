﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using InternetBanking.Models;
using InternetBanking.Services;
using Microsoft.AspNetCore.Mvc;

namespace InternetBanking.Controllers
{
    public class HomeController : Controller
    {
        private readonly IGeneratePassword generatePasswordService;

        public HomeController()
        {
            
            generatePasswordService = new GeneratePasswordService();
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

      
    }
}
