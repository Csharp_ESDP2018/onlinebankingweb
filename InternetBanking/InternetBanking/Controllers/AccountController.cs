﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternetBanking.Models;
using InternetBanking.Services;
using InternetBanking.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InternetBanking.ViewModels.Paging;

namespace InternetBanking.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService accountService;
        private readonly ISelectListService selectListService;
        private readonly ApplicationContext context;
        private readonly UserManager<User> userManager;
        private readonly IHomePagingService pagingService;
        private readonly ICompanyService companyService;
        private readonly IUserService userService;

        public AccountController(IAccountService accountService, ISelectListService selectListService, ApplicationContext context, UserManager<User> userManager
            , IHomePagingService pagingService, ICompanyService companyService, IUserService userService)
        {
            this.accountService = accountService;
            this.selectListService = selectListService;
            this.context = context;
            this.userManager = userManager;
            this.pagingService = pagingService;
            this.companyService = companyService;
            this.userService= userService;
        }

        public IActionResult UserAccountCreate(string userId)
        {
          
            CreateUserAccountViewModel model = new CreateUserAccountViewModel
            {
               Currencies = selectListService.GetCurrencies(),
               UserId = userId
            };

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> UserAccountCreate(CreateUserAccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await userManager.FindByIdAsync(model.UserId);
                Currency currency = await context.Currencies.FirstOrDefaultAsync(c => c.Id == model.CurrencyId);
                Account account = accountService.CreateAccount(user, currency);
                accountService.AddAccount(account);
                return RedirectToAction("UserAccounts", "Account", new { userId = model.UserId });
            }
            model.Currencies = selectListService.GetCurrencies();
            return View(model);
        }

        public IActionResult CompanyAccountCreate(int companyId)
        {

            CompanyAccountCreateViewModel model = new CompanyAccountCreateViewModel
            {
                Currencies = selectListService.GetCurrencies(),
                CompanyId = companyId
            };

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CompanyAccountCreate(CompanyAccountCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                Company company = await context.Companies.FirstOrDefaultAsync(c => c.Id == model.CompanyId);
                Currency currency = await context.Currencies.FirstOrDefaultAsync(c => c.Id == model.CurrencyId);
                Account account = accountService.CreateAccount(company, currency);
                accountService.AddAccount(account);
                return RedirectToAction("Index", "Account" , new { companyId = model.CompanyId});
            }
            model.Currencies = selectListService.GetCurrencies();
            return View(model);
        }

        public async Task<ActionResult> Index(int companyId, int page = 1)
        {
            List<AccountWithBalance> accounts = accountService.GetCompanyAccounts(companyId);
            
            ViewBag.OwnerId = companyId;
            ViewBag.OwnerName = companyService.FindCompanyById(companyId).NameCompany;

            return View(accounts);
        }

        public async Task<ActionResult> UserAccounts(string userId, int page = 1)
        {
            UserInfo user = await userService.FindUserInfoByUserId(userId);
            List<AccountWithBalance> accounts = accountService.GetUserInfoAccounts(user.Id);
            UserAccountViewModel userAccounts = new UserAccountViewModel { UserInfo = user, Accounts = accounts };

            return View(userAccounts);
        }

        [HttpGet]
        public async Task<IActionResult> AccountInfo(int id)
        {
            Account currentAccount = await accountService.FindAccountById(id);
            AccountWithBalance accountWithBalance = new AccountWithBalance()
            {
                Account = currentAccount,
                Balance = accountService.GetAccountBalance(currentAccount).Result
            };

            return View(accountWithBalance);
        }
    }
}