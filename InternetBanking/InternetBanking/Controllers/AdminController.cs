﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternetBanking.Models;
using InternetBanking.Services;
using InternetBanking.ViewModels;
using InternetBanking.ViewModels.Paging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InternetBanking.Controllers
{
    public class AdminController : Controller
    {

        private ApplicationContext context;

        public AdminController(ApplicationContext context)

        {

            this.context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> PasswordBlackList()
        {
            if (!User.IsInRole("admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            List<BlackList> blackLists = context.BlackListedPasswords.ToList();
            return View(blackLists);
        }

       

        [Authorize(Roles = "admin")]
        public IActionResult CreateBlackList()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public IActionResult CreateBlackList(BlackListPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {

                BlackList blackListedPassword = new BlackList()
                {
                    Id = model.Id,
                    BlackListedPassword = model.Name
                };
                context.BlackListedPasswords.Add(blackListedPassword);
                context.SaveChanges();
                return RedirectToAction("PasswordBlackList");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Заполните поле");
            }

            return View(model);
        }


        [Authorize(Roles = "admin")]
        public IActionResult EditBlackList(int id)
        {
            BlackList blackList = context.BlackListedPasswords.Find(id);

            BlackListPasswordViewModel model = new BlackListPasswordViewModel
            {

                Id = blackList.Id,
                Name = blackList.BlackListedPassword
            };

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public IActionResult EditBlackList(BlackListPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {

                BlackList blackList = new BlackList()
                {
                    Id = model.Id,
                    BlackListedPassword = model.Name
                };

                context.BlackListedPasswords.Update(blackList);
                context.SaveChanges();
                return RedirectToAction("PasswordBlackList");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Заполните поле");
            }

            return View(model);
        }
        [Authorize(Roles = "admin")]
        public IActionResult DeleteBlackLisedPassword(int id)
        {
            BlackList blackList = context.BlackListedPasswords.Find(id);
            context.BlackListedPasswords.Remove(blackList);

            return RedirectToAction("PasswordBlackList");
        }

    }
}







