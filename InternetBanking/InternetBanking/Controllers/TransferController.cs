﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternetBanking.Models;
using InternetBanking.Services;
using InternetBanking.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace InternetBanking.Controllers
{
    [Authorize]
    public class TransferController : Controller
    {
        private readonly ISelectListService selectListService;
        private readonly IUserService userService;
        private readonly ITransferService transferService;
        private readonly IAccountService accountService;
        private readonly IStringLocalizer<TransferController> localizer;

        public TransferController(ISelectListService selectListService, IUserService userService, ITransferService transferService, IAccountService accountService, IStringLocalizer<TransferController> localizer)
        {
            this.selectListService = selectListService;
            this.userService = userService;
            this.transferService = transferService;
            this.accountService = accountService;
            this.localizer = localizer;
        }


        public IActionResult InnerTransfer()
        {
            string userName = String.Empty;
            int userId = 0;
            User user = userService.FindUserByName(HttpContext.User.Identity.Name);
            UserInfo userInfo = userService.FindUserByIdInUserInfo(user.Id, ref userName, ref userId);
            InnerTransferViewModel model = new InnerTransferViewModel();
            model.UserAccounts = selectListService.GetUserAccounts(userInfo.Id);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> InnerTransfer(InnerTransferViewModel model)
        {
            double amount = 0;

            User user = userService.FindUserByName(HttpContext.User.Identity.Name);

            if (model.AccountSenderId == null)
            {
                ModelState.AddModelError("AccountSenderId", localizer["AccountSenderIdValidation"]);
            }
            else
            {
                if (!await accountService.IsAccountExist(model.ReceiverAccountNumber))
                {
                    ModelState.AddModelError("ReceiverAccountNumber", localizer["ReceiverAccountNumberExistVlidation"]);

                }
                else if (await accountService.IsAccountExist(model.ReceiverAccountNumber) && await accountService.IsAccountSenderNotReceiver(model.ReceiverAccountNumber, user))
                {
                    ModelState.AddModelError("ReceiverAccountNumber", localizer["IsAccountSenderNotReceiverValidation"]);
                }
                else if (await accountService.IsAccountExist(model.ReceiverAccountNumber) && await accountService.CompareAccountsCurrencies(model.ReceiverAccountNumber, (int)model.AccountSenderId))
                {
                    ModelState.AddModelError("ReceiverAccountNumber", localizer["CurrencyValidation"]);
                }
                if (!accountService.AmountTryParse(model.Amount, out amount))
                {
                    ModelState.AddModelError("Amount", localizer["AmountFormatValidation"]);
                }
                else if (amount <= 0)
                {
                    ModelState.AddModelError("Amount", localizer["AmountNotNull"]);
                }
                else if (!await accountService.IsBalanceEnough((int)model.AccountSenderId, amount))
                {
                    ModelState.AddModelError("Amount", localizer["IsBalanceEnoughValidation"]);
                }
            }



            if (ModelState.IsValid)
            {
                Account sender = accountService.FindAccountById((int)model.AccountSenderId).Result;
                Account receiver = accountService.FindAccountByNumber(model.ReceiverAccountNumber).Result;

                InnerTransfer transfer =
                    transferService.CreateInnerTransfer(sender, receiver, amount, model.Comment);
                transferService.AddTransfer(transfer);
                return RedirectToAction("Transfer", "Transfer");
            }

            string userName = String.Empty;
            int userId = 0;

            UserInfo userInfo = userService.FindUserByIdInUserInfo(user.Id, ref userName, ref userId);
            model.UserAccounts = selectListService.GetUserAccounts(userInfo.Id);
            return View(model);
        }

        public IActionResult Transfer()
        {
            return View();
        }
        [HttpGet]
        public IActionResult AddMoneyUserAccount(int accountId)
        {
            Account account = accountService.FindAccountById(accountId).Result;
            AddMoneyViewModel model = new AddMoneyViewModel { Account = account};
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddMoneyUserAccount(AddMoneyViewModel model)
        {
            Account sender = null;
            Account receiver = accountService.FindAccountById(model.Account.Id).Result;
            string comment = string.Empty;
            double amount = 0;
               
                if (!accountService.AmountTryParse(model.Amount, out amount))
                {
                    ModelState.AddModelError("Amount", localizer["AmountFormatValidation"]);
                }
                else if (amount <= 0)
                {
                    ModelState.AddModelError("Amount", localizer["AmountNotNull"]);
                }

            if (ModelState.IsValid)
            {

                InnerTransfer transfer =
                    transferService.CreateInnerTransfer(sender, receiver, amount, comment);
                transferService.AddTransfer(transfer);
                if (receiver.UserInfoId!= null)
                {
                    return RedirectToAction("UserAccounts", "Account", new { userId = receiver.UserInfo.User.Id});
                }
                else if (receiver.CompanyId != null)
                {
                    return RedirectToAction("Index", "Account", new { companyId = receiver.CompanyId });
                }

            }
            model.Account = receiver;
            return View(model);
        }
    }

}