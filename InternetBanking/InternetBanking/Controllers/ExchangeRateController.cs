﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using InternetBanking.Models;
using InternetBanking.ViewModels;
using Microsoft.EntityFrameworkCore;
using InternetBanking.Services;
using Microsoft.Extensions.Localization;

namespace InternetBanking.Controllers
{
    public class ExchangeRateController : Controller
    {
        private readonly ICurrencyService currencyService;
        private readonly IExchangeRateService exchangeRateService;
        private readonly IStringLocalizer<ExchangeRateController> localizer;

        public ExchangeRateController(ICurrencyService currencyService, IExchangeRateService exchangeRateService
            , IStringLocalizer<ExchangeRateController> localizer)
        {
            this.currencyService = currencyService;
            this.exchangeRateService = exchangeRateService;
            this.localizer = localizer;
        }

        [Authorize]
        public ActionResult Index()
        {
            if (!User.IsInRole("admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            ExchangeRatesListViewModel model = new ExchangeRatesListViewModel()
            {
                exchangeRatesList = exchangeRateService.GetLastExchangeRatesByDate(),
                currencyList = currencyService.GetCurrencies().ToList()
            };

            return View(model);
        }

        public ActionResult Create()
        {
            ExchangeRateViewModel model = new ExchangeRateViewModel()
            {
                CurrencyId = -1,
                CurrencyList = currencyService.GetCurrencies().ToList(),
                TypeId = -1,
                TypeList = exchangeRateService.GetExchangeRateTypes().ToList()
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ExchangeRateViewModel model)
        {
            double rate = 0;

            if (!RateTryParse(model.Rate, out rate))
            {
                ModelState.AddModelError("Rate", localizer["RateFormatValidation"]);
            }
            else if (rate <= 0)
            {
                ModelState.AddModelError("Rate", localizer["RateNotNull"]);
            }

            if (ModelState.IsValid)
            {
                ExchangeRate exchangeRate = new ExchangeRate()
                {
                    Rate = rate,
                    RateDate = DateTime.Now,
                    CurrencyId = model.CurrencyId,
                    ExchangeRateTypeId = model.TypeId
                };

                exchangeRateService.AddExchangeRate(exchangeRate);

                return RedirectToAction("Index", "ExchangeRate");
            }

            model.CurrencyList = currencyService.GetCurrencies().ToList();
            model.TypeList = exchangeRateService.GetExchangeRateTypes().ToList();
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ExchangeRate exchangeRate = exchangeRateService.FindExchangeRateById(id);

            ExchangeRateViewModel model = new ExchangeRateViewModel()
            {
                Id = id,
                CurrencyId = exchangeRate.CurrencyId,
                Rate = exchangeRate.Rate.ToString(),
                CurrencyList = currencyService.GetCurrencies().ToList(),
                TypeId = exchangeRate.ExchangeRateTypeId,
                TypeList = exchangeRateService.GetExchangeRateTypes().ToList()
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ExchangeRateViewModel model)
        {
            double rate = 0;

            if (!RateTryParse(model.Rate, out rate))
            {
                ModelState.AddModelError("Rate", localizer["RateFormatValidation"]);
            }
            else if (rate <= 0)
            {
                ModelState.AddModelError("Rate", localizer["RateNotNull"]);
            }

            if (ModelState.IsValid)
            {
                ExchangeRate exchangeRate = exchangeRateService.FindExchangeRateById(model.Id);
                exchangeRate.Rate = rate;
                exchangeRate.CurrencyId = model.CurrencyId;
                exchangeRate.ExchangeRateTypeId = model.TypeId;
                exchangeRate.RateDate = DateTime.Now;

                exchangeRateService.UpdateExchangeRate(exchangeRate);

                return RedirectToAction("Index", "ExchangeRate");
            }

            model.CurrencyList = currencyService.GetCurrencies().ToList();
            model.TypeList = exchangeRateService.GetExchangeRateTypes().ToList();
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            ExchangeRate exchangeRate = exchangeRateService.FindExchangeRateById(id);
            exchangeRateService.RemoveExchangeRate(exchangeRate);

            return RedirectToAction("Index", "ExchangeRate");
        }

        public bool RateTryParse(string modelAmount, out double amount)
        {
            if (!string.IsNullOrEmpty(modelAmount))
            {
                modelAmount = modelAmount.Replace('.', ',');
            }
            bool tryParse = double.TryParse(modelAmount, out amount);
            amount = Math.Round(amount, 2);

            return tryParse;
        }
    }
}