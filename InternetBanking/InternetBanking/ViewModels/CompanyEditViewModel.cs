﻿using InternetBanking.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InternetBanking.ViewModels
{
    public class CompanyEditViewModel
    {
        public FactAddressViewModel FactAddress { get; set; }
        public LegalAddressViewModel LegalAddress { get; set; }

        public CompanyEditViewModel()
        {
            FactAddress = new FactAddressViewModel();
            LegalAddress = new LegalAddressViewModel();
        }

        
        public SelectList Countries { set; get; }

       
        public SelectList LegalForms { set; get; }

  
        public SelectList PropertyTypes { set; get; }

     
        public SelectList Residencies { set; get; }

      
        public SelectList TaxInspections { set; get; }

        public CompanyViewModel Company { get; set; }
        public RegistrationDataViewModel RegistrationData { get; set; }

        public IFormFile Logo { get; set; }
    }
}
